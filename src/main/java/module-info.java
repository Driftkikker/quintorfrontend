module login.loginform {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires spring.boot;
    requires com.fasterxml.jackson.databind;
    requires json.schema.validator;
    requires java.xml;
    requires org.json;
    requires spring.web;
    requires org.apache.commons.io;

    opens login.loginform to javafx.fxml, javafx.base;
    opens login.loginform.model to javafx.fxml, javafx.base;
    exports login.loginform;
    exports login.loginform.controllers;
    opens login.loginform.controllers to javafx.fxml;
}