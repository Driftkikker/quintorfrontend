package login.loginform.validator;

public enum SchemaType {
    STATEMENT,
    TRANSACTION
}
