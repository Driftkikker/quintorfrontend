package login.loginform.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import login.loginform.model.ContentType;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.Set;

public class Validator {

    private final static String transactionSchemaXSD = "src/main/java/login/loginform/validator/schemas/transaction.xsd";
    private final static String transactionSchemaJSON = "src/main/java/login/loginform/validator/schemas/transaction.json";
    private final static String statementSchemaXSD = "src/main/java/login/loginform/validator/schemas/statement.xsd";
    private final static String statementSchemaJSON = "src/main/java/login/loginform/validator/schemas/statement.json";

    /**
     * Validate a MT940 XML string with XSD
     *
     * @param xml MT940 XML
     * @return boolean valid
     */
    public static boolean validateXML(String xml, SchemaType schemaType) {
        try {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(typeToSchema(schemaType));
            javax.xml.validation.Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new StringReader(xml)));
        } catch (IOException | SAXException e) {
            System.err.println(e);
            return false;
        }
        return true;
    }

    /**
     * Validate a MT940 JSON string with Draft-07
     *
     * @param json MT940 JSON
     * @return boolean valid
     */
    public static boolean validateJSON(String json, SchemaType schemaType) {
        try {
            File schema = typeToSchema(schemaType);
            if (schema == null) throw new IllegalArgumentException("Invalid schema");

            InputStream schemaStream = new FileInputStream(schema);
            InputStream jsonStream = new ByteArrayInputStream(json.getBytes());

            ObjectMapper streamToNodeMapper = new ObjectMapper();
            JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V7);
            JsonSchema jsonSchema = factory.getSchema(schemaStream);
            JsonNode jsonNode = streamToNodeMapper.readTree(jsonStream);

            Set<ValidationMessage> errors = jsonSchema.validate(jsonNode);

            if (errors.isEmpty()) return true;

            for (ValidationMessage error : errors) {
                System.err.println("ValidationMessage: " + error);
            }
            return false;

        } catch (Exception e) {
            System.err.println(e);
            return false;
        }
    }

    /**
     * Convert SchemaType to schema
     *
     * @param schemaType type of schema
     * @return File with the schema
     */
    private static File typeToSchema(SchemaType schemaType) {
        switch (schemaType) {
            case TRANSACTION -> {
                return new File(
                        ContentType.currentContentType == ContentType.XML ?
                                transactionSchemaXSD : transactionSchemaJSON
                );
            }
            case STATEMENT -> {
                return new File(
                        ContentType.currentContentType == ContentType.XML ?
                                statementSchemaXSD : statementSchemaJSON
                );
            }
            default -> {
                System.err.println("Schema not found");
                return null;
            }
        }
    }

}
