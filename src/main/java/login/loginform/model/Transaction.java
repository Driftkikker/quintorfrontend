package login.loginform.model;

public class Transaction {
    private int id;
    private String value_date;
    private String entry_date;
    private String debit_credit_mark;
    private String amount;
    private String transaction_type;
    private String reference_account_owner;
    private String reference_account_institution;
    private String supplementary_details;
    private String rentdata_info;
    private String bardata_info;
    private String rentdata_start_date;
    private String rentdata_end_date;
    private String description;
    private String narrative;
    private int statement_id;

    public Transaction(int id, String value_date, String entry_date, String debit_credit_mark, String amount, String transaction_type, String reference_account_owner, String reference_account_institution, String supplementary_details, String rentdata_info, String bardata_info, String rentdata_start_date, String rentdata_end_date, String description, String narrative, int statementId) {
        this.id = id;
        this.value_date = value_date;
        this.entry_date = entry_date;
        this.debit_credit_mark = debit_credit_mark;
        this.amount = amount;
        this.transaction_type = transaction_type;
        this.reference_account_owner = reference_account_owner;
        this.reference_account_institution = reference_account_institution;
        this.supplementary_details = supplementary_details;
        this.rentdata_info = rentdata_info;
        this.bardata_info = bardata_info;
        this.rentdata_start_date = rentdata_start_date;
        this.rentdata_end_date = rentdata_end_date;
        this.description = description;
        this.narrative = narrative;
        statement_id = statementId;
    }

    public int getId() {
        return this.id;
    }

    public String getValue_date() {
        return this.value_date;
    }

    public String getEntry_date() {
        return this.entry_date;
    }

    public String getDebit_credit_mark() {
        return this.debit_credit_mark;
    }

    public String getAmount() {
        return this.amount;
    }

    public String getTransaction_type() {
        return this.transaction_type;
    }

    public String getReference_account_owner() {
        return this.reference_account_owner;
    }

    public String getReference_account_institution() {
        return this.reference_account_institution;
    }

    public String getSupplementary_details() {
        return this.supplementary_details;
    }

    public String getRentdata_info() {
        return this.rentdata_info;
    }

    public String getBardata_info() {
        return this.bardata_info;
    }

    public String getRentdata_start_date() {
        return this.rentdata_start_date;
    }

    public String getRentdata_end_date() {
        return this.rentdata_end_date;
    }

    public int getStatement_id() {
        return statement_id;
    }

    public String getDescription() {
        return description;
    }

    public String getNarrative() {
        return narrative;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", value_date='" + value_date + '\'' +
                ", entry_date='" + entry_date + '\'' +
                ", debit_credit_mark='" + debit_credit_mark + '\'' +
                ", amount='" + amount + '\'' +
                ", transaction_type='" + transaction_type + '\'' +
                ", reference_account_owner='" + reference_account_owner + '\'' +
                ", reference_account_institution='" + reference_account_institution + '\'' +
                ", supplementary_details='" + supplementary_details + '\'' +
                ", rentdata_info='" + rentdata_info + '\'' +
                ", bardata_info='" + bardata_info + '\'' +
                ", rentdata_start_date='" + rentdata_start_date + '\'' +
                ", rentdata_end_date='" + rentdata_end_date + '\'' +
                '}';
    }
}
