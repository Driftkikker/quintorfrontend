package login.loginform.model;

public class Statement {
    int id;
    String account_holder_info;
    String account_identification;
    String application_id;
    String block_type;
    String direction;
    String logical_terminal;
    String message_type;
    String receiver_address;
    String sequence_number;
    String service_id;
    String session_number;
    String statement_number;
    String transaction_reference_number;
    int user_id;
    Balance final_balance;
    Balance future_available_balance;
    Balance start_balance;
    Balance available_final_balance;

    public Statement(int id, String account_holder_info, String account_identification, String application_id, String block_type, String direction, String logical_terminal, String message_type, String reciever_adress, String sequence_number, String service_id, String session_number, String statement_number, Balance final_balance, Balance furute_available_balance, Balance start_balance, Balance avaiable_final_balance, String transaction_reference_number, int user_id) {
        this.id = id;
        this.account_holder_info = account_holder_info;
        this.account_identification = account_identification;
        this.application_id = application_id;
        this.block_type = block_type;
        this.direction = direction;
        this.logical_terminal = logical_terminal;
        this.message_type = message_type;
        this.receiver_address = reciever_adress;
        this.sequence_number = sequence_number;
        this.service_id = service_id;
        this.session_number = session_number;
        this.statement_number = statement_number;
        this.final_balance = final_balance;
        this.future_available_balance = furute_available_balance;
        this.start_balance = start_balance;
        this.available_final_balance = avaiable_final_balance;
        this.transaction_reference_number = transaction_reference_number;
        this.user_id = user_id;
    }

    public int getId() {
        return this.id;
    }

    public String getAccount_holder_info() {
        return this.account_holder_info;
    }

    public String getAccount_identification() {
        return this.account_identification;
    }

    public String getApplication_id() {
        return this.application_id;
    }

    public String getBlock_type() {
        return this.block_type;
    }

    public String getDirection() {
        return this.direction;
    }

    public String getLogical_terminal() {
        return this.logical_terminal;
    }

    public String getMessage_type() {
        return this.message_type;
    }

    public String getReceiver_address() {
        return this.receiver_address;
    }

    public String getSequence_number() {
        return this.sequence_number;
    }

    public String getService_id() {
        return this.service_id;
    }

    public String getSession_number() {
        return this.session_number;
    }

    public String getStatement_number() {
        return this.statement_number;
    }

    public Balance getFinal_balance() {
        return this.final_balance;
    }

    public Balance getFuture_available_balance() {
        return this.future_available_balance;
    }

    public Balance getStart_balance() {
        return this.start_balance;
    }

    public Balance getAvailable_final_balance() {
        return this.available_final_balance;
    }

    public String getTransaction_reference_number() {
        return this.transaction_reference_number;
    }

    public int getUser_id() {
        return this.user_id;
    }

    @Override
    public String toString() {
        return "Statement{" +
                "id=" + id +
                ", account_holder_info='" + account_holder_info + '\'' +
                ", account_identification='" + account_identification + '\'' +
                ", application_id='" + application_id + '\'' +
                ", block_type='" + block_type + '\'' +
                ", direction='" + direction + '\'' +
                ", logical_terminal='" + logical_terminal + '\'' +
                ", message_type='" + message_type + '\'' +
                ", receiver_address='" + receiver_address + '\'' +
                ", sequence_number='" + sequence_number + '\'' +
                ", service_id='" + service_id + '\'' +
                ", session_number='" + session_number + '\'' +
                ", statement_number='" + statement_number + '\'' +
                ", transaction_reference_number='" + transaction_reference_number + '\'' +
                ", user_id=" + user_id +
                ", final_balance=" + final_balance +
                ", future_available_balance=" + future_available_balance +
                ", start_balance=" + start_balance +
                ", available_final_balance=" + available_final_balance +
                '}';
    }
}
