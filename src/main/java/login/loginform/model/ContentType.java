package login.loginform.model;

public enum ContentType {
    XML,
    JSON;

    public static ContentType currentContentType;
}
