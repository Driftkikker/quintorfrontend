package login.loginform.model;

public class Balance {
    String status;
    String amount;
    String currency;
    String date;

    public Balance(String status, String amount, String currency, String date) {
        this.status = status;
        this.amount = amount;
        this.currency = currency;
        this.date = date;
    }

    public String getStatus() {
        return this.status;
    }

    public String getAmount() {
        return this.amount;
    }

    public String getCurrency() {
        return this.currency;
    }

    public String getDate() {
        return this.date;
    }

    public String toXML() {
        return "<balance>" +
                "<status>" + status + "<status>" +
                "<amount>" + amount + "<amount>" +
                "<currency>" + currency + "<currency>" +
                "<date>" + status + "<date>" +
                "</balance>";
    }
}
