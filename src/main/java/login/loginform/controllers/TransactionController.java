package login.loginform.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import login.loginform.Application;
import login.loginform.model.Balance;
import login.loginform.model.ContentType;
import login.loginform.model.Statement;
import login.loginform.model.Transaction;
import login.loginform.validator.SchemaType;
import login.loginform.validator.Validator;
import login.loginform.validator.ValidatorException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class TransactionController {
    @FXML
    private TextArea description_box;
    @FXML
    private Label statement_content;
    @FXML
    private Label transaction_content;
    @FXML
    private TextArea barinfo_box;
    @FXML
    private TextArea rentinfo_box;
    @FXML
    private DatePicker rentstart_datepicker;
    @FXML
    private DatePicker rentend_datepicker;

    private static Transaction transaction;

    @FXML
    protected void initialize() {
        transaction_content.setText("Value date: " + TransactionController.transaction.getValue_date() + "\n" +
                "Entry date: " + TransactionController.transaction.getEntry_date() + "\n" +
                "Debit-credit mark: " + TransactionController.transaction.getDebit_credit_mark() + "\n" +
                "Amount: " + TransactionController.transaction.getAmount() + "\n" +
                "Transaction type: " + TransactionController.transaction.getTransaction_type() + "\n" +
                "Reference account owner: " + TransactionController.transaction.getReference_account_owner() + "\n" +
                "Reference account institution: " + TransactionController.transaction.getReference_account_institution() + "\n" +
                "Supplementary details: " + TransactionController.transaction.getSupplementary_details() + "\n" +
                "Narrative: " + TransactionController.transaction.getNarrative()
        );
        description_box.setText(TransactionController.transaction.getDescription());

        barinfo_box.setText(TransactionController.transaction.getBardata_info());
        rentinfo_box.setText(TransactionController.transaction.getRentdata_info());

        String rentStart = TransactionController.transaction.getRentdata_start_date();
        String rentEnd = TransactionController.transaction.getRentdata_end_date();

        if (!rentStart.isEmpty() && !rentStart.equals("null"))
            rentstart_datepicker.setValue(LocalDate.parse(rentStart));
        if (!rentEnd.isEmpty() && !rentEnd.equals("null"))
            rentend_datepicker.setValue(LocalDate.parse(rentEnd));

        try {
            String url = "http://localhost:9000/getStatement/" + TransactionController.transaction.getStatement_id() + "/" + ContentType.currentContentType.toString().toLowerCase();
            String result = APIController.getAPIResponse(url);

            boolean isValid = ContentType.currentContentType == ContentType.JSON ? Validator.validateJSON(result, SchemaType.STATEMENT) : Validator.validateXML(result, SchemaType.STATEMENT);

            if (isValid) {
                Statement statement = ContentType.currentContentType == ContentType.JSON ? statementFromJSON(result) : statementFromXML(result);

                if (statement != null) {
                    statement_content.setText(
                            "application_id: " + statement.getApplication_id() + "\n" +
                                    "service_id: " + statement.getService_id() + "\n" +
                                    "logical_terminal: " + statement.getLogical_terminal() + "\n" +
                                    "session_number: " + statement.getSession_number() + "\n" +
                                    "sequence_number: " + statement.getSequence_number() + "\n" +
                                    "reciever_adress: " + statement.getReceiver_address() + "\n" +
                                    "message_type: " + statement.getMessage_type() + "\n" +
                                    "block_type: " + statement.getBlock_type() + "\n" +
                                    "direction: " + statement.getDirection() + "\n" +
                                    "transaction_reference_number: " + statement.getTransaction_reference_number() + "\n" +
                                    "account_identification: " + statement.getAccount_identification() + "\n" +
                                    "statement_number: " + statement.getStatement_number() + "\n" +
                                    "account_holder_information: " + statement.getAccount_holder_info() + "\n\n" +
                                    "start_balance: \n" +
                                    "\tstatus: " + statement.getStart_balance().getStatus() + "\n" +
                                    "\tamount: " + statement.getStart_balance().getAmount() + "\n" +
                                    "\tcurrency: " + statement.getStart_balance().getCurrency() + "\n" +
                                    "\tdate: " + statement.getStart_balance().getDate() + "\n\n" +
                                    "final_balance: \n" +
                                    "\tstatus: " + statement.getFinal_balance().getStatus() + "\n" +
                                    "\tamount: " + statement.getFinal_balance().getAmount() + "\n" +
                                    "\tcurrency: " + statement.getFinal_balance().getCurrency() + "\n" +
                                    "\tdate: " + statement.getFinal_balance().getDate() + "\n\n" +
                                    "available_final_balance: \n" +
                                    "\tstatus: " + statement.getAvailable_final_balance().getStatus() + "\n" +
                                    "\tamount: " + statement.getAvailable_final_balance().getAmount() + "\n" +
                                    "\tcurrency: " + statement.getAvailable_final_balance().getCurrency() + "\n" +
                                    "\tdate: " + statement.getAvailable_final_balance().getDate() + "\n\n" +
                                    "future_available_balance: \n" +
                                    "\tstatus: " + statement.getFuture_available_balance().getStatus() + "\n" +
                                    "\tamount: " + statement.getFuture_available_balance().getAmount() + "\n" +
                                    "\tcurrency: " + statement.getFuture_available_balance().getCurrency() + "\n" +
                                    "\tdate: " + statement.getFuture_available_balance().getDate() + "\n\n"
                    );
                } else {
                    statement_content.setText("Could not load statement data");
                }
            } else {
                throw new ValidatorException("Invalid statement for ContentType: " + ContentType.currentContentType);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static void setTransaction(Transaction transaction) {
        TransactionController.transaction = transaction;
    }

    private Statement statementFromJSON(String json) {
        JSONObject statementJson = new JSONObject(json);

        return new Statement(
                Integer.parseInt(statementJson.get("id").toString()),
                statementJson.get("account_holder_info").toString(),
                statementJson.get("account_identification").toString(),
                statementJson.get("application_id").toString(),
                statementJson.get("block_type").toString(),
                statementJson.get("direction").toString(),
                statementJson.get("logical_terminal").toString(),
                statementJson.get("message_type").toString(),
                statementJson.get("receiver_address").toString(),
                statementJson.get("sequence_number").toString(),
                statementJson.get("service_id").toString(),
                statementJson.get("session_number").toString(),
                statementJson.get("statement_number").toString(),
                new Balance(
                        statementJson.getJSONObject("final_balance").get("status").toString(),
                        statementJson.getJSONObject("final_balance").get("amount").toString(),
                        statementJson.getJSONObject("final_balance").get("currency").toString(),
                        statementJson.getJSONObject("final_balance").get("date").toString()
                ),
                new Balance(
                        statementJson.getJSONObject("future_available_balance").get("status").toString(),
                        statementJson.getJSONObject("future_available_balance").get("amount").toString(),
                        statementJson.getJSONObject("future_available_balance").get("currency").toString(),
                        statementJson.getJSONObject("future_available_balance").get("date").toString()
                ),
                new Balance(
                        statementJson.getJSONObject("start_balance").get("status").toString(),
                        statementJson.getJSONObject("start_balance").get("amount").toString(),
                        statementJson.getJSONObject("start_balance").get("currency").toString(),
                        statementJson.getJSONObject("start_balance").get("date").toString()
                ),
                new Balance(
                        statementJson.getJSONObject("available_final_balance").get("status").toString(),
                        statementJson.getJSONObject("available_final_balance").get("amount").toString(),
                        statementJson.getJSONObject("available_final_balance").get("currency").toString(),
                        statementJson.getJSONObject("available_final_balance").get("date").toString()
                ),
                statementJson.get("transaction_reference_number").toString(),
                Integer.parseInt(statementJson.get("user_id").toString()));
    }

    private Statement statementFromXML(String xml) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new ByteArrayInputStream(xml.getBytes()));

            return new Statement(
                    Integer.parseInt(doc.getElementsByTagName("id").item(0).getTextContent()),
                    doc.getElementsByTagName("account_holder_info").item(0).getTextContent(),
                    doc.getElementsByTagName("account_identification").item(0).getTextContent(),
                    doc.getElementsByTagName("application_id").item(0).getTextContent(),
                    doc.getElementsByTagName("block_type").item(0).getTextContent(),
                    doc.getElementsByTagName("direction").item(0).getTextContent(),
                    doc.getElementsByTagName("logical_terminal").item(0).getTextContent(),
                    doc.getElementsByTagName("message_type").item(0).getTextContent(),
                    doc.getElementsByTagName("receiver_address").item(0).getTextContent(),
                    doc.getElementsByTagName("sequence_number").item(0).getTextContent(),
                    doc.getElementsByTagName("service_id").item(0).getTextContent(),
                    doc.getElementsByTagName("session_number").item(0).getTextContent(),
                    doc.getElementsByTagName("statement_number").item(0).getTextContent(),
                    new Balance(
                            ((Element) doc.getElementsByTagName("final_balance").item(0)).getElementsByTagName("status").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("final_balance").item(0)).getElementsByTagName("amount").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("final_balance").item(0)).getElementsByTagName("currency").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("final_balance").item(0)).getElementsByTagName("date").item(0).getTextContent()
                    ),
                    new Balance(
                            ((Element) doc.getElementsByTagName("future_available_balance").item(0)).getElementsByTagName("status").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("future_available_balance").item(0)).getElementsByTagName("amount").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("future_available_balance").item(0)).getElementsByTagName("currency").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("future_available_balance").item(0)).getElementsByTagName("date").item(0).getTextContent()
                    ),
                    new Balance(
                            ((Element) doc.getElementsByTagName("start_balance").item(0)).getElementsByTagName("status").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("start_balance").item(0)).getElementsByTagName("amount").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("start_balance").item(0)).getElementsByTagName("currency").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("start_balance").item(0)).getElementsByTagName("date").item(0).getTextContent()
                    ),
                    new Balance(
                            ((Element) doc.getElementsByTagName("available_final_balance").item(0)).getElementsByTagName("status").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("available_final_balance").item(0)).getElementsByTagName("amount").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("available_final_balance").item(0)).getElementsByTagName("currency").item(0).getTextContent(),
                            ((Element) doc.getElementsByTagName("available_final_balance").item(0)).getElementsByTagName("date").item(0).getTextContent()
                    ),
                    doc.getElementsByTagName("transaction_reference_number").item(0).getTextContent(),
                    Integer.parseInt(doc.getElementsByTagName("user_id").item(0).getTextContent()));
        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
            return null;
        }
    }


    @FXML
    private void update() {
        String description = description_box.getText();
        String bartext = barinfo_box.getText();
        String renttext = rentinfo_box.getText();
        String start_date = rentstart_datepicker.getValue() == null ? "" : rentstart_datepicker.getValue().toString();
        String end_date = rentend_datepicker.getValue() == null ? "" : rentend_datepicker.getValue().toString();

        if (!description.equals(TransactionController.transaction.getDescription())) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                URI postURI = new URI("http://localhost:9000/updateDescription/" + TransactionController.transaction.getId() + "/" + ContentType.currentContentType.toString().toLowerCase());
                ResponseEntity<String> output = restTemplate.postForEntity(postURI, description, String.class);

                if (output.getStatusCode() == HttpStatus.OK)
                    PageController.redirectPage("dashboard");
                else
                    System.err.println("Failed to update description, Error code: " + output.getStatusCode());
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println(e);
            }
        }
        if (!bartext.equals(TransactionController.transaction.getBardata_info())) {
            try {
                RestTemplate restTemplate = new RestTemplate();

                String call = String.format("http://localhost:9000/updateBar/%d/%b/%s",
                        TransactionController.transaction.getId(),
                        TransactionController.transaction.getBardata_info().isEmpty(),
                        ContentType.currentContentType.toString().toLowerCase()
                );
                URI postURI = new URI(call);

                ResponseEntity<String> output = restTemplate.postForEntity(postURI, bartext, String.class);

                if (output.getStatusCode() == HttpStatus.OK)
                    PageController.redirectPage("dashboard");
                else
                    System.err.println("Failed to update bar data, Error code: " + output.getStatusCode());
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println(e);
            }
        }
        if (!renttext.equals(TransactionController.transaction.getRentdata_info()) ||
                !start_date.equals(TransactionController.transaction.getRentdata_start_date()) ||
                !end_date.equals(TransactionController.transaction.getRentdata_end_date())) {
            if (!renttext.isEmpty() && !start_date.isEmpty() && !end_date.isEmpty()) {

                try {
                    RestTemplate restTemplate = new RestTemplate();

                    String call = String.format("http://localhost:9000/updateRent/%d/%b/%s",
                            TransactionController.transaction.getId(),
                            TransactionController.transaction.getRentdata_info().isEmpty(),
                            ContentType.currentContentType.toString().toLowerCase()
                    );
                    URI postURI = new URI(call);

                    JSONObject input = new JSONObject();
                    input.put("description", renttext);
                    input.put("start_date", start_date);
                    input.put("end_date", end_date);

                    ResponseEntity<String> output = restTemplate.postForEntity(postURI, input.toString(), String.class);

                    if (output.getStatusCode() == HttpStatus.OK)
                        PageController.redirectPage("dashboard");
                    else
                        System.err.println("Failed to update rent data, Error code: " + output.getStatusCode());
                } catch (Exception e) {
                    e.printStackTrace();
                    System.err.println(e);
                }
            } else {
                System.err.println("Empty fields not allowed");
            }
        }
    }
}
