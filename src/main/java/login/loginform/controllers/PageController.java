package login.loginform.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import login.loginform.Application;

import java.io.IOException;
import java.util.Objects;
import java.util.Stack;

public class PageController {
    private final static int APPLICATION_WIDTH = 1000;
    private final static int APPLICATION_HEIGHT = 700;

    private static final Stack<Scene> previousScenes = new Stack<>();

    public static Scene redirectPage(String filename) throws IOException {
        Stage stage = Application.window;
        Parent root = FXMLLoader.load(Objects.requireNonNull(Application.class.getResource(filename + ".fxml")));
        previousScenes.add(stage.getScene());
        Scene scene = new Scene(root,
                APPLICATION_WIDTH,
                APPLICATION_HEIGHT
        );

        scene.addEventFilter(KeyEvent.KEY_PRESSED, ke -> {
            if (ke.getCode() == KeyCode.LEFT) {
                previousStage();
                ke.consume(); // <-- stops passing the event to next node
            }
        });

        stage.setScene(scene);
        stage.show();
        return scene;
    }

    public static void previousStage() {
        Stage stage = Application.window;
        stage.setScene(previousScenes.pop());
        stage.show();
    }
}
