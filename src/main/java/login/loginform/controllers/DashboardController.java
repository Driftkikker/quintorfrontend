package login.loginform.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import login.loginform.Application;
import login.loginform.model.ContentType;
import login.loginform.model.FilterType;
import login.loginform.model.Transaction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.io.FileUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import login.loginform.validator.SchemaType;
import login.loginform.validator.Validator;
import login.loginform.validator.ValidatorException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class DashboardController {
    @FXML
    private Button changeContentTypeBtn;
    @FXML
    private TableView<Transaction> tableView;
    @FXML
    private TableColumn<Transaction, String> valueDate;
    @FXML
    private TableColumn<Transaction, String> entryDate;
    @FXML
    private TableColumn<Transaction, String> debitOrCredit;
    @FXML
    private TableColumn<Transaction, String> amount;
    @FXML
    private TableColumn<Transaction, String> accountOwner;

    @FXML
    private Button searchBtn;
    @FXML
    private TextField searchField;
    @FXML
    private Button sortAllBtn;
    @FXML
    private Button sortRentBtn;
    @FXML
    private Button sortBarBtn;
    @FXML
    private DatePicker startDatePicker;
    @FXML
    private DatePicker endDatePicker;

    /**
     * Called when the page is loaded
     */
    @FXML
    protected void initialize() {
        searchBtn.setOnAction(e -> searchEvent());
        sortAllBtn.setOnAction(e -> displayTransactions());
        sortRentBtn.setOnAction(e -> filterEvent(FilterType.RENT));
        sortBarBtn.setOnAction(e -> filterEvent(FilterType.BAR));
        startDatePicker.setOnAction(e -> filterDate());
        endDatePicker.setOnAction(e -> filterDate());

        changeContentTypeBtn.setText(ContentType.currentContentType.toString());

        displayTransactions();
    }

    private void displayTransactions() {
        sortAllBtn.setStyle("-fx-background-color: #43CC4D");
        sortAllBtn.setTextFill(Color.web("0xFFFFFF"));
        sortRentBtn.setStyle("-fx-background-color: #FFFFFF");
        sortRentBtn.setTextFill(Color.web("0x43CC4D"));
        sortBarBtn.setStyle("-fx-background-color: #FFFFFF");
        sortBarBtn.setTextFill(Color.web("0x43CC4D"));

        try {
            List<Transaction> transactions = this.getTransactions();
            this.populateTable(transactions);
        } catch (IOException ioe) {
            System.err.println("Encountered a problem while retrieving transactions from the database: " + ioe);
            ioe.printStackTrace();
        } catch (ValidatorException e) {
            System.err.println("Encountered a problem while validation api request: " + e);
            e.printStackTrace();
        }
    }

    private void resetButtonStyles() {
        sortAllBtn.setStyle("-fx-background-color: #FFFFFF");
        sortAllBtn.setTextFill(Color.web("0x43CC4D"));
        sortRentBtn.setStyle("-fx-background-color: #FFFFFF");
        sortRentBtn.setTextFill(Color.web("0x43CC4D"));
        sortBarBtn.setStyle("-fx-background-color: #FFFFFF");
        sortBarBtn.setTextFill(Color.web("0x43CC4D"));
    }

    private void filterEvent(FilterType filterType) {
        if (filterType == FilterType.RENT) {
            sortAllBtn.setStyle("-fx-background-color: #FFFFFF");
            sortAllBtn.setTextFill(Color.web("0x43CC4D"));
            sortRentBtn.setStyle("-fx-background-color: #43CC4D");
            sortRentBtn.setTextFill(Color.web("0xFFFFFF"));
            sortBarBtn.setStyle("-fx-background-color: #FFFFFF");
            sortBarBtn.setTextFill(Color.web("0x43CC4D"));
        } else if (filterType == FilterType.BAR) {
            sortAllBtn.setStyle("-fx-background-color: #FFFFFF");
            sortAllBtn.setTextFill(Color.web("0x43CC4D"));
            sortRentBtn.setStyle("-fx-background-color: #FFFFFF");
            sortRentBtn.setTextFill(Color.web("0x43CC4D"));
            sortBarBtn.setStyle("-fx-background-color: #43CC4D");
            sortBarBtn.setTextFill(Color.web("0xFFFFFF"));
        }

        try {
            List<Transaction> allTransactions = getTransactions();
            List<Transaction> filteredTransactions = new ArrayList<>();

            for (Transaction transaction : allTransactions) {
                String data = filterType == FilterType.RENT ? transaction.getRentdata_info() : transaction.getBardata_info();

                if (!data.equals("null")) {
                    filteredTransactions.add(transaction);
                }
            }
            this.populateTable(filteredTransactions);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    /**
     * helper method to load the transactions into the tableview
     *
     * @param transactions List of Transaction objects to put into the table
     */
    private void populateTable(List<Transaction> transactions) {
        tableView.setRowFactory(tv -> {
            TableRow<Transaction> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Transaction rowData = row.getItem();
                    redirectPage(rowData);
                }
            });
            return row;
        });

        valueDate.setCellValueFactory(new PropertyValueFactory<>("value_date"));
        entryDate.setCellValueFactory(new PropertyValueFactory<>("entry_date"));
        debitOrCredit.setCellValueFactory(new PropertyValueFactory<>("debit_credit_mark"));
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        accountOwner.setCellValueFactory(new PropertyValueFactory<>("reference_account_owner"));

        tableView.getItems().clear();
        tableView.getItems().setAll(transactions);
    }

    /**
     * Gets transactions from the API and reads them with scanner object
     *
     * @return List of transaction objects
     * @throws IOException        Throws when API connection has an error
     * @throws ValidatorException Throws when API response is invalid
     */
    private List<Transaction> getTransactions() throws IOException, ValidatorException {
        String data = APIController.getAPIResponse(
                "http://localhost:9000/getTransactions/" + ContentType.currentContentType.toString().toLowerCase());

        boolean isValid;

        isValid = (ContentType.currentContentType == ContentType.JSON) ?
                Validator.validateJSON(data, SchemaType.TRANSACTION) :
                Validator.validateXML(data, SchemaType.TRANSACTION);

        if (isValid)
            return (ContentType.currentContentType == ContentType.JSON) ?
                    parseJSONTransactionList(data) :
                    parseXMLTransactionList(data);
        else
            throw new ValidatorException("Unable to receive api data: invalid " + ContentType.currentContentType);
    }

    private List<Transaction> filterTransaction(String filterString) throws ValidatorException, IOException {
        List<Transaction> allTransactions = getTransactions();
        List<Transaction> filteredTransactions = new ArrayList<>();

        if (filterString.isEmpty()) filteredTransactions = allTransactions;

        for (Transaction transaction : allTransactions) {
            if (transaction.getBardata_info().contains(filterString) ||
                    transaction.getRentdata_info().contains(filterString) ||
                    transaction.getReference_account_institution().contains(filterString) ||
                    transaction.getSupplementary_details().contains(filterString) ||
                    transaction.getReference_account_owner().contains(filterString)
            ) {
                filteredTransactions.add(transaction);
            }
        }

        return filteredTransactions;
    }

    /**
     * Parses the database result by turning the JSON String into transaction objects
     *
     * @return list of transactions
     */
    private List<Transaction> parseJSONTransactionList(String jsonString) {
        JSONArray transactionJsonArray = new JSONArray(jsonString);

        ArrayList<Transaction> rows = new ArrayList<>();

        for (int i = 0; i < transactionJsonArray.length(); i++) {
            JSONObject transactionJson = transactionJsonArray.getJSONObject(i);

            rows.add(new Transaction(
                    Integer.parseInt(transactionJson.get("id").toString()),
                    transactionJson.get("value_date").toString(),
                    transactionJson.get("entry_date").toString(),
                    transactionJson.get("debit_credit_mark").toString(),
                    transactionJson.get("amount").toString(),
                    transactionJson.get("transaction_type").toString(),
                    transactionJson.get("reference_account_owner").toString(),
                    transactionJson.get("reference_account_institution").toString(),
                    transactionJson.get("supplementary_details").toString(),
                    transactionJson.get("rentdata_info").toString().equals("null") ? "" :  transactionJson.get("rentdata_info").toString(),
                    transactionJson.get("bardata_info").toString().equals("null") ? "" :  transactionJson.get("bardata_info").toString(),
                    transactionJson.get("rentdata_start_date").toString().equals("null") ? "" :  transactionJson.get("rentdata_start_date").toString(),
                    transactionJson.get("rentdata_end_date").toString().equals("null") ? "" :  transactionJson.get("rentdata_end_date").toString(),
                    transactionJson.get("description").toString().equals("null") ? "" :  transactionJson.get("description").toString(),
                    transactionJson.get("narrative").toString().equals("null") ? "" :  transactionJson.get("narrative").toString(),
                    Integer.parseInt(transactionJson.get("statement_id").toString())));
        }

        return rows;
    }

    /**
     * Parses the database result by turning the XML String into transaction objects
     *
     * @return list of transactions
     */
    private List<Transaction> parseXMLTransactionList(String xmlString) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new ByteArrayInputStream(xmlString.getBytes()));

            ArrayList<Transaction> rows = new ArrayList<>();

            NodeList list = doc.getElementsByTagName("transaction");
            for (int i = 0; i < list.getLength(); i++) {
                Element item = (Element) list.item(i);

                rows.add(new Transaction(
                        Integer.parseInt(item.getElementsByTagName("id").item(0).getTextContent()),
                        item.getElementsByTagName("value_date").item(0).getTextContent(),
                        item.getElementsByTagName("entry_date").item(0).getTextContent(),
                        item.getElementsByTagName("debit_credit_mark").item(0).getTextContent(),
                        item.getElementsByTagName("amount").item(0).getTextContent(),
                        item.getElementsByTagName("transaction_type").item(0).getTextContent(),
                        item.getElementsByTagName("reference_account_owner").item(0).getTextContent(),
                        item.getElementsByTagName("reference_account_institution").item(0).getTextContent(),
                        item.getElementsByTagName("supplementary_details").item(0).getTextContent(),
                        item.getElementsByTagName("rentdata_info").item(0).getTextContent(),
                        item.getElementsByTagName("bardata_info").item(0).getTextContent(),
                        item.getElementsByTagName("rentdata_start_date").item(0).getTextContent(),
                        item.getElementsByTagName("rentdata_end_date").item(0).getTextContent(),
                        item.getElementsByTagName("description").item(0).getTextContent(),
                        item.getElementsByTagName("narrative").item(0).getTextContent(),
                        Integer.parseInt(item.getElementsByTagName("statement_id").item(0).getTextContent())));
            }

            return rows;

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e);
            return new ArrayList<>();
        }
    }

    public void filterDate() {
        resetButtonStyles();

        LocalDate startDate = startDatePicker.getValue();
        LocalDate endDate = endDatePicker.getValue();

        try {
            List<Transaction> allTransactions = getTransactions();
            List<Transaction> filteredTransactions = new ArrayList<>();

            for (Transaction transaction : allTransactions) {
                LocalDate transactionDate = LocalDate.parse(transaction.getValue_date());

                if (startDate != null) {
                    if (transactionDate.isAfter(startDate) || transactionDate.isEqual(startDate)) {
                        if (endDate != null) {
                            if (transactionDate.isBefore(endDate) || transactionDate.isEqual(endDate))
                                filteredTransactions.add(transaction);
                        } else {
                            filteredTransactions.add(transaction);
                        }
                    }
                }
            }


            populateTable(filteredTransactions);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public void searchEvent() {
        resetButtonStyles();

        String searchBoxContent = searchField.getText();

        try {
            List<Transaction> transactions;

            if (searchBoxContent.isEmpty()) {
                transactions = this.getTransactions();
            } else {
                transactions = this.filterTransaction(searchBoxContent);
            }
            this.populateTable(transactions);
        } catch (Exception e) {
            System.err.println("Search error:" + e.getMessage());
        }
    }

    public void upload() {
        try {
            FileChooser fil_chooser = new FileChooser();
            File file = fil_chooser.showOpenDialog(Application.window);

            String mt940String = FileUtils.readFileToString(file, "UTF-8");
            RestTemplate restTemplate = new RestTemplate();
            URI postURI = new URI("http://localhost:9000/addStatement/" + ContentType.currentContentType.toString().toLowerCase());
            ResponseEntity<String> output = restTemplate.postForEntity(postURI, mt940String, String.class);

            initialize();

            if (!output.getStatusCode().equals(HttpStatus.OK)) {
                System.err.println("upload failed");
            }
        } catch (IOException ioe) {
            System.err.println("couldn't read file");
        } catch (URISyntaxException use) {
            System.err.println("invalid upload URI syntax");
        } catch (NullPointerException npe) {
            System.err.println("No file selected");
        }
    }

    private void redirectPage(Transaction transaction) {
        try {
            TransactionController.setTransaction(transaction);
            PageController.redirectPage("transaction");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Dashboard controller redirect page: " + e);
        }
    }

    @FXML
    private void changeContentType() {
        if (ContentType.currentContentType == ContentType.JSON) {
            ContentType.currentContentType = ContentType.XML;
            changeContentTypeBtn.setText("XML");
        } else {
            ContentType.currentContentType = ContentType.JSON;
            changeContentTypeBtn.setText("JSON");
        }
    }
}
