package login.loginform.controllers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class APIController {
    public static String getAPIResponse(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = con.getResponseCode();

        if (responseCode == 404) {
            System.err.println("nothing found");
            throw new IOException("Could not find API");
        } else if (responseCode != 200) {
            System.err.println("Error with api");
            throw new IOException("Error while connecting to API: Code " + responseCode);
        }

        Scanner scanner = new Scanner(url.openStream());
        StringBuilder apiResult = new StringBuilder();
        while (scanner.hasNext()) {
            apiResult.append(scanner.nextLine());
        }

        return apiResult.toString();
    }
}
