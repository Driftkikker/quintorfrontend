package login.loginform.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import login.loginform.Application;
import login.loginform.model.ContentType;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginController {

    @FXML
    protected TextField userName;
    @FXML
    protected PasswordField password;

    @FXML
    protected void loginButtonHandler() throws IOException {
        URL url = new URL("http://localhost:9000/login/" + Application.user);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        if(con.getResponseCode() == 200)
            redirectPage();
    }

    private void redirectPage() {
        try {
            PageController.redirectPage("dashboard");

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e);
        }
    }
}