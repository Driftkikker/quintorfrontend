package login.loginform;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import login.loginform.controllers.PageController;
import login.loginform.model.ContentType;

import java.io.IOException;
import java.util.Objects;

public class Application extends javafx.application.Application {

    public static Stage window;
    public static String user;

    @Override
    public void start(Stage stage) throws IOException {
        window = stage;
        window.setResizable(false);
        window.setTitle("QFinance");
        stage.getIcons().add(
                new Image(
                        Objects.requireNonNull(getClass().getResourceAsStream("/login/icon.png"))));

        ContentType.currentContentType = ContentType.JSON;
        Application.user = "paymaster";

        PageController.redirectPage("main");
    }

    public static void main(String[] args) {
        launch();
    }
}